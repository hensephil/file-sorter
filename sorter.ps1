### Erstellt am 01.04.2021 - Philipp Hense ###
param (
 [string] $Dateien,
 [string] $Zielpfad 
)


# Log Funktion
function Write-Log
{
    Param
    (
        $text,
        $Zielpfad
    )

    $Zielpfadneu = "$Zielpfad" + "\log.log"
    "$(get-date -format "yyyy-MM-dd H:mm:ss"): $($text)" | out-file $Zielpfadneu -Append
}



# Holt sich die Dateien, die ohne Ordner verschoben werden sollen
$files = Get-ChildItem $Dateien -Recurse -Exclude *.log | where {!$_.PsIsContainer}

# Listen Sie die Dateien auf, die verschoben werden sollen
$files

# Zieldatei, in den Dateien verschoben werden sollen. Das Skript erstellt automatisch einen Ordner f�r das Jahr und den Monat.
$targetPath = $Zielpfad

foreach ($file in $files)
{
# Finde das Jahr und den Monat wann die Datei erstellt wurde
# Ich habe LastWriteTime verwendet, da es sich um synchronisierte Dateien handelt und der Erstellungstag das Datum ist, an dem die Synchronisierung durchgef�hrt wurde
$year = $file.LastWriteTime.Year.ToString()
if ($file.LastWriteTime.Month -lt 10)
{
$month ="0"+$file.LastWriteTime.Month.ToString()
}
else{
$month = $file.LastWriteTime.Month.ToString()
 }

# Out FileName, Jahr und Monat
$file.Name
$year
$month


# Verzeichnispfad festlegen
#neuer Weg mit Jahr und Monat
 $Directory = $targetPath + "\" + $year + "\" + $month

# Erstellen Sie ein Verzeichnis, wenn es nicht vorhanden ist
if (!(Test-Path $Directory))
{
New-Item $directory -type directory
}

# Log schreiben
$Logtext = "Die Datei '" + $file + "' wurde in '" + $Directory + "' verschoben."

Write-Log ($Zielpfad) -text $Logtext -Zielpfad  $Zielpfad


# Verschiebe Dateien in den Ordner
$file | Move-Item -Destination $Directory
}
