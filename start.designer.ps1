### Erstellt am 01.04.2021 - Philipp Hense ###

$Form1 = New-Object -TypeName System.Windows.Forms.Form
[System.Windows.Forms.Button]$save = $null
[System.Windows.Forms.Button]$abourt = $null
[System.Windows.Forms.PictureBox]$PictureBox1 = $null
[System.Windows.Forms.Label]$titel = $null
[System.Windows.Forms.Label]$Label1 = $null
[System.Windows.Forms.TextBox]$TextBox1 = $null
[System.Windows.Forms.Label]$Label2 = $null
[System.Windows.Forms.Label]$Label3 = $null
[System.Windows.Forms.TextBox]$TextBox2 = $null
function InitializeComponent
{
$resources = . (Join-Path $PSScriptRoot 'start.resources.ps1')
$save = (New-Object -TypeName System.Windows.Forms.Button)
$abourt = (New-Object -TypeName System.Windows.Forms.Button)
$PictureBox1 = (New-Object -TypeName System.Windows.Forms.PictureBox)
$titel = (New-Object -TypeName System.Windows.Forms.Label)
$Label1 = (New-Object -TypeName System.Windows.Forms.Label)
$TextBox1 = (New-Object -TypeName System.Windows.Forms.TextBox)
$Label2 = (New-Object -TypeName System.Windows.Forms.Label)
$Label3 = (New-Object -TypeName System.Windows.Forms.Label)
$TextBox2 = (New-Object -TypeName System.Windows.Forms.TextBox)
([System.ComponentModel.ISupportInitialize]$PictureBox1).BeginInit()
$Form1.SuspendLayout()
#
#save
#
$save.Location = (New-Object -TypeName System.Drawing.Point -ArgumentList @([System.Int32]322,[System.Int32]296))
$save.Name = [System.String]'save'
$save.Size = (New-Object -TypeName System.Drawing.Size -ArgumentList @([System.Int32]75,[System.Int32]23))
$save.TabIndex = [System.Int32]0
$save.Text = [System.String]'Starten'
$save.UseCompatibleTextRendering = $true
$save.UseVisualStyleBackColor = $true
$save.add_Click($Button1_Click)
#
#abourt
#
$abourt.Location = (New-Object -TypeName System.Drawing.Point -ArgumentList @([System.Int32]241,[System.Int32]296))
$abourt.Name = [System.String]'abourt'
$abourt.Size = (New-Object -TypeName System.Drawing.Size -ArgumentList @([System.Int32]75,[System.Int32]23))
$abourt.TabIndex = [System.Int32]1
$abourt.Text = [System.String]'Abbrechen'
$abourt.UseCompatibleTextRendering = $true
$abourt.UseVisualStyleBackColor = $true
$abourt.add_Click($Button1_Click)
#
#PictureBox1
#
$PictureBox1.Image = ([System.Drawing.Image]$resources.'PictureBox1.Image')
$PictureBox1.Location = (New-Object -TypeName System.Drawing.Point -ArgumentList @([System.Int32]322,[System.Int32]12))
$PictureBox1.Name = [System.String]'PictureBox1'
$PictureBox1.Size = (New-Object -TypeName System.Drawing.Size -ArgumentList @([System.Int32]83,[System.Int32]85))
$PictureBox1.TabIndex = [System.Int32]2
$PictureBox1.TabStop = $false
#
#titel
#
$titel.Font = (New-Object -TypeName System.Drawing.Font -ArgumentList @([System.String]'Tahoma',[System.Single]14.25,[System.Drawing.FontStyle]::Bold,[System.Drawing.GraphicsUnit]::Point,([System.Byte][System.Byte]0)))
$titel.Location = (New-Object -TypeName System.Drawing.Point -ArgumentList @([System.Int32]128,[System.Int32]12))
$titel.Name = [System.String]'titel'
$titel.Size = (New-Object -TypeName System.Drawing.Size -ArgumentList @([System.Int32]111,[System.Int32]23))
$titel.TabIndex = [System.Int32]3
$titel.Text = [System.String]'File Sorter'
$titel.UseCompatibleTextRendering = $true
#
#Label1
#
$Label1.Location = (New-Object -TypeName System.Drawing.Point -ArgumentList @([System.Int32]12,[System.Int32]99))
$Label1.Name = [System.String]'Label1'
$Label1.Size = (New-Object -TypeName System.Drawing.Size -ArgumentList @([System.Int32]264,[System.Int32]23))
$Label1.TabIndex = [System.Int32]4
$Label1.Text = [System.String]'Speicherort der Datein die Sortiert werden sollen'
$Label1.UseCompatibleTextRendering = $true
#
#TextBox1
#
$TextBox1.Location = (New-Object -TypeName System.Drawing.Point -ArgumentList @([System.Int32]12,[System.Int32]116))
$TextBox1.Name = [System.String]'TextBox1'
$TextBox1.Size = (New-Object -TypeName System.Drawing.Size -ArgumentList @([System.Int32]385,[System.Int32]21))
$TextBox1.TabIndex = [System.Int32]5
#
#Label2
#
$Label2.ForeColor = [System.Drawing.Color]::Gray
$Label2.Location = (New-Object -TypeName System.Drawing.Point -ArgumentList @([System.Int32]-1,[System.Int32]314))
$Label2.Name = [System.String]'Label2'
$Label2.Size = (New-Object -TypeName System.Drawing.Size -ArgumentList @([System.Int32]132,[System.Int32]23))
$Label2.TabIndex = [System.Int32]6
$Label2.Text = [System.String]'� 2021 Philipp Hense'
$Label2.UseCompatibleTextRendering = $true
$Label2.add_Click($Label2_Click)
#
#Label3
#
$Label3.Location = (New-Object -TypeName System.Drawing.Point -ArgumentList @([System.Int32]12,[System.Int32]153))
$Label3.Name = [System.String]'Label3'
$Label3.Size = (New-Object -TypeName System.Drawing.Size -ArgumentList @([System.Int32]385,[System.Int32]32))
$Label3.TabIndex = [System.Int32]7
$Label3.Text = [System.String]'Speicherort, in den Dateien verschoben werden sollen. Das Skript erstellt automatisch einen Ordner fuer das Jahr und den Monat.'
$Label3.UseCompatibleTextRendering = $true
$Label3.add_Click($Label3_Click)
#
#TextBox2
#
$TextBox2.Location = (New-Object -TypeName System.Drawing.Point -ArgumentList @([System.Int32]12,[System.Int32]188))
$TextBox2.Name = [System.String]'TextBox2'
$TextBox2.Size = (New-Object -TypeName System.Drawing.Size -ArgumentList @([System.Int32]385,[System.Int32]21))
$TextBox2.TabIndex = [System.Int32]8
#
#Form1
#
$Form1.ClientSize = (New-Object -TypeName System.Drawing.Size -ArgumentList @([System.Int32]409,[System.Int32]331))
$Form1.Controls.Add($TextBox2)
$Form1.Controls.Add($Label3)
$Form1.Controls.Add($Label2)
$Form1.Controls.Add($TextBox1)
$Form1.Controls.Add($Label1)
$Form1.Controls.Add($titel)
$Form1.Controls.Add($PictureBox1)
$Form1.Controls.Add($abourt)
$Form1.Controls.Add($save)
$Form1.Icon = ([System.Drawing.Icon]$resources.'$this.Icon')
$Form1.Text = [System.String]'File Sorter'
([System.ComponentModel.ISupportInitialize]$PictureBox1).EndInit()
$Form1.ResumeLayout($false)
$Form1.PerformLayout()
Add-Member -InputObject $Form1 -Name base -Value $base -MemberType NoteProperty
Add-Member -InputObject $Form1 -Name save -Value $save -MemberType NoteProperty
Add-Member -InputObject $Form1 -Name abourt -Value $abourt -MemberType NoteProperty
Add-Member -InputObject $Form1 -Name PictureBox1 -Value $PictureBox1 -MemberType NoteProperty
Add-Member -InputObject $Form1 -Name titel -Value $titel -MemberType NoteProperty
Add-Member -InputObject $Form1 -Name Label1 -Value $Label1 -MemberType NoteProperty
Add-Member -InputObject $Form1 -Name TextBox1 -Value $TextBox1 -MemberType NoteProperty
Add-Member -InputObject $Form1 -Name Label2 -Value $Label2 -MemberType NoteProperty
Add-Member -InputObject $Form1 -Name Label3 -Value $Label3 -MemberType NoteProperty
Add-Member -InputObject $Form1 -Name TextBox2 -Value $TextBox2 -MemberType NoteProperty
}
. InitializeComponent
