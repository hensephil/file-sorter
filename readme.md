# File Sorter

Sortiert Dateien nach dem Jahr und Monat

## Erste Schritte

Lade dir zunächst die Dateien herunter.
Gehe mit rechtsklick auf die Powershell Datei und sage "öffnen mit" (Vergiss nicht den Hacken zu setzen das es immer so ausgeführt wird) Powershell


### Benutzung
Öffne die Start.cmd 

Es öffnet sich eine Powwershell GUI in der Sie den Pfad angeben in dem die Unsortierten Dateien liegen.
Ebenfalls geben Sie den Ort an in dem die Dateien abgelegt werden sollen.
Nicht wundern wärend des kompletten Vorgangs sieht es so aus als würde das Programm sich aufgehängt haben.

## Autor

* **Philipp Hense** - (https://gitlab.com/hensephil)


